﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradesPrototype.Data
{
    // Types of user
    public enum Role { Teacher, Student };

    // TODO: Exercise 2: Task 1a: Create the Grade struct
    public struct Grade
    {
        public int StudentID;
        public string AssessmentDate;
        public string SubjectName;
        public string Assessment;
        public string Comments;
    }


    // TODO: Exercise 2: Task 1b: Create the Student struct
    public struct Student
    {
        public int StudentID;
        public string UserName;
        public string Password;
        public int TeacherID;
        public string FirstName;
        public string LastName;
    }

    // TODO: Exercise 2: Task 1c: Create the Teacher struct
    public struct Teacher
    {
        public int TeacherID;
        public string UserName;
        public string Password;
        public string FirstName;
        public string LastName;
        public string Class;
    }
}
