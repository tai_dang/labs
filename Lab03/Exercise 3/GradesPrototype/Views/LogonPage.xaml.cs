﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Views
{
    /// <summary>
    /// Interaction logic for LogonPage.xaml
    /// </summary>
    public partial class LogonPage : UserControl
    {
        public LogonPage()
        {
            InitializeComponent();
        }

        #region Event Members
        public event EventHandler LogonSuccess;

        // TODO: Exercise 3: Task 1a: Define LogonFailed event
        public delegate void LogonFailedEventHandler(object sender, LoginFailedEventArgs e);
        public event LogonFailedEventHandler LogonFailed;

        #endregion

        #region Logon Validation

        // TODO: Exercise 3: Task 1b: Validate the username and password against the Users collection in the MainWindow window
        private void Logon_Click(object sender, RoutedEventArgs e)
        {
            var teacher = (from Teacher t in DataSource.Teachers
                          where t.UserName == username.Text
                          select t).FirstOrDefault();

            Student student = new Student();
            if (teacher.Equals(default(Teacher)))
            {
                student = (from Student s in DataSource.Students
                              where s.UserName == username.Text
                              select s).FirstOrDefault();
            }


            if (teacher.Equals(default(Teacher)) && student.Equals(default(Student)))
            {
                // User doesn't exist
                LogonFailed(this, new LoginFailedEventArgs("Login Failed! User doesn't exist."));
                return;
            }

            string pw;

            if (!teacher.Equals(default(Teacher)))
            {
                SessionContext.UserName = teacher.UserName;
                SessionContext.CurrentTeacher = teacher;
                SessionContext.UserRole = Role.Teacher;
                pw = teacher.Password;
            }
            else
            {
                SessionContext.UserName = student.UserName;
                SessionContext.CurrentStudent = student;
                SessionContext.UserRole = Role.Student;
                pw = student.Password;
            }

            if (pw != password.Password)
            {
                // Password is wrong
                LogonFailed(this, new LoginFailedEventArgs("Login Failed! Password incorrect!"));
                return;
            }

            LogonSuccess?.Invoke(this, null);
        }
        #endregion
    }
    public class LoginFailedEventArgs : EventArgs
    {
        public string Message { get; set; }

        public LoginFailedEventArgs(string message)
        {
            Message = message;
        }
    }
}
