﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradesPrototype.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradesPrototype.Data.Tests
{
    [TestClass()]
    public class GradeTests
    {
        [TestInitialize]
        public void Init()
        {
            DataSource.CreateData();
        }

        [TestMethod()]
        [DataRow(1, "12-Feb-21", "Math", "B", "Very Good")]
        [DataRow(12, "31-Dec-20", "History", "D-", "Try more")]
        [DataRow(50, "16-Jul-91", "Geography", "E+", "Very bad")]
        public void TestValidGrade(int studentID, string assessmentDate, string subject, string assessment, string comments)
        {
            var grade = new Grade(studentID, assessmentDate, subject, assessment, comments);

            Assert.IsInstanceOfType(grade, typeof(Grade));

            Assert.AreEqual(grade.StudentID, studentID);
            Assert.AreEqual(grade.AssessmentDate, assessmentDate);
            Assert.AreEqual(grade.SubjectName, subject);
            Assert.AreEqual(grade.Assessment, assessment);
            Assert.AreEqual(grade.Comments, comments);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestBadDate()
        {
            var grade = new Grade();

            grade.AssessmentDate = (DateTime.Today + new TimeSpan(2, 0, 0, 0)).ToString("d");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        [DataRow("12-12-21")]
        [DataRow("31-12-20")]
        [DataRow("31-13-20")]
        [DataRow("15/12/2011")]
        public void TestDateNotRecognized(String assessmentDate)
        {
            var grade = new Grade();

            grade.AssessmentDate = assessmentDate;
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        [DataRow("DD")]
        [DataRow("F+")]
        [DataRow("Z-")]
        [DataRow("H")]
        [DataRow("")]
        public void TestBadAssessment(String assessment)
        {
            var grade = new Grade();

            grade.Assessment = assessment;
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        [DataRow("Mathematics")]
        public void TestBadSubject(String subject)
        {
            var grade = new Grade();

            grade.SubjectName = subject;
        }
    }
}